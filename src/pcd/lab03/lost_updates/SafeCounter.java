package pcd.lab03.lost_updates;

public class SafeCounter extends UnsafeCounter {

	private int cont;
	
	public SafeCounter(int base){
		super(base);
	}
	
	public void inc(){
		synchronized(this){
			cont++;
		}
	}
	
	public int getValue(){
		synchronized (this){
			return cont;
		}
	}
}
